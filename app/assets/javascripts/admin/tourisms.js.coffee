# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

map = null
autocomplete = null
marker =  null

setMapByFind = ->
  marker.setMap(null) if marker?
  place = autocomplete.getPlace()
  location = autocomplete.getPlace().geometry.location

  map.setCenter(new google.maps.LatLng(location.lat(), location.lng()));
  marker = new google.maps.Marker(
    position: map.getCenter()
    map: map
    draggable: true
  )

  google.maps.event.addListener marker, "drag", ->
    $("#tourism_lat").val marker.position.lat()
    $("#tourism_lng").val marker.position.lng()
    return

  i = 0

  while i < place.address_components.length
    console.log
    if place.address_components[i].types[0] is "country"
      $("#tourism_country").val place.address_components[i].long_name
    else if place.address_components[i].types[0] is "administrative_area_level_1"
      $("#tourism_region").val place.address_components[1].long_name
    else if place.address_components[i].types[0] is "locality"
      $("#tourism_city").val place.address_components[1].long_name
    i++
  $("#tourism_address").val place.formatted_address
  $("#tourism_lat").val location.lat()
  $("#tourism_lng").val location.lng()


setMap = ->
  mapOptions =
    zoom: 10
    center: new google.maps.LatLng(-6.9148644, 107.60824209999998)

  map = new google.maps.Map(document.getElementById("map-tourism"), mapOptions)
  autocomplete = new google.maps.places.Autocomplete(document.getElementById("find-location-map"));
  google.maps.event.addListener(autocomplete, 'place_changed', setMapByFind);

google.maps.event.addDomListener window, "load", setMap