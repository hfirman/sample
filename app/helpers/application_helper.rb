module ApplicationHelper
  def title(value)
    unless value.nil?
      @title = "#{value} | RoverTrip"      
    end
  end
end
