class CreateTourisms < ActiveRecord::Migration
  def change
    create_table :tourisms do |t|
      t.string :name
      t.string :country
      t.string :region
      t.string :city
      t.string :address
      t.string :description
      t.string :lat
      t.string :lng

      t.timestamps
    end
  end
end
